# Roachat
A CLI where you can debug with an actual bug. Inspired by
[this](https://www.youtube.com/watch?v=qflMWrcefGE).

## How to use:
1. `$ roachat`
2. Enter some text, and you'll get a random response.

Example:
```
$ roachat
Another bug, huh? What is it this time?
> So I have this apple in my game, but I everytime I remove it, my game
completely breaks.
It's a feature, not a bug. ;)
> Fuck off. ^C
```

## How to obtain:
### Prebuilt binaries:
Check the releases section for a prebuilt binary that runs on your OS/Arch
combination.

### Build from source:

#### C:
1. If you're on Windows, you can get either:
	* the [Visual Studio build tools](https://visualstudio.microsoft.com/downloads),
	* or [MinGW](https://sourceforge.net/projects/mingw).
2. If you're on UNIX, you probably already have a compiler. If not, there's [GCC](https://gcc.gnu.org), [Clang](https://clang.llvm.org), and [TCC](https://bellard.org/tcc).
3. Compile `src/main.c` with your compiler.

#### Rust:
- Just build: `cargo build --release`
- Build and install: `cargo install --path .`
