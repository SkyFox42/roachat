#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define BUF 100

int main(void) {
	char* prompts[] = {
		"Another bug, huh? What is it this time?",
		"What's the matter?",
		"Another bug?",
		"What's the problem?",
		"Alright, tell me what's wrong."
	};

	char* responses[] = {
		"Hmm...",
		"I see.",
		"Go on.",
		"Okay.",
		"Have you tried rewriting it in Rust?"
	};

	char userInput[BUF];
	time_t t;
	srand((unsigned) time(&t)); // Use seconds after epoch as seed for rand().
	printf("%s\n", prompts[rand() % (sizeof(prompts) / sizeof(prompts[0]))]);
	/* sizeof(arr) / sizeof(arr[0]) gives you the total amount of elements in
	arr[]. */

	while (printf("> "), fflush(stdout), fgets(userInput, BUF, stdin)) {
		printf("%s\n", responses[rand() % (sizeof(responses) / sizeof(responses[0]))]);
	}

	return 0;
}
