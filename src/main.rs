use rand::random;
use std::io::{stdin, stdout, Write};

fn main() {
    let prompts = [
        "Another bug, huh? What is it this time?",
        "What's the matter?",
        "Another bug?",
        "What's the problem?",
        "Alright, tell me what's wrong.",
    ];

    let responses = [
        "Hmm...",
        "I see.",
        "Go on.",
        "Okay.",
        "Have you tried rewriting it in Rust?",
        "Have you tried 🦀?",
    ];

    println!(
        "{}",
        prompts.get(random::<usize>() % prompts.len()).unwrap()
    );

    let mut input = String::new();

    loop {
        print!("> ");
        stdout().flush().expect("Error: failed to flush stdout");
        stdin()
            .read_line(&mut input)
            .expect("Error: unable to read user input");
        println!(
            "{}",
            responses.get(random::<usize>() % responses.len()).unwrap()
        );
    }
}
